# 推演组前端集成系统 | Deduction Frontend System

## 介绍
本项目基于 Vite+Vue3+TS 搭建的前端集成系统项目，内置各种插件及其使用案例作为参考。<br/>
由ISCAS推演组前端团队维护支持。

项目预览：[https://zero-dg.gitee.io/deduction-frontend](https://zero-dg.gitee.io/deduction-frontend)

## 项目源码下载
``` sh
git clone https://gitee.com/zero-dg/deduction-frontend.git
```

## 运行&部署

``` sh
#下载后安装依赖 
npm i
#本地运行
npm run dev
#部署打包
npm run build
```

## 项目配置详细介绍

项目搭建方式和详细使用细则，请访问[推演组前端集成系统文档](https://zero-dg.gitee.io/is-deduction-docs/)
