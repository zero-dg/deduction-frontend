var P=Object.defineProperty;var T=(r,e,t)=>e in r?P(r,e,{enumerable:!0,configurable:!0,writable:!0,value:t}):r[e]=t;var o=(r,e,t)=>(T(r,typeof e!="symbol"?e+"":e,t),t);import{e as F,s as g,E as z,o as I,c as D,j as b,a as w,J as y,ak as k,al as A,h as _}from"./index.8bfb7b00.js";import{E as L}from"./el-button.b6cdfad1.js";import{_ as B}from"./_plugin-vue_export-helper.cdc0426e.js";class V{constructor(e,t={animation:!0,baseLayerPicker:!0,fullscreenButton:!1,geocoder:!1,homeButton:!0,timeline:!0,sceneModePicker:!0,sceneMode:Cesium.SceneMode.SCENE3D,navigationHelpButton:!1,selectionIndicator:!0,infoBox:!1,orderIndependentTranslucency:!0}){o(this,"viewer");Cesium.Camera.DEFAULT_VIEW_RECTANGLE=Cesium.Rectangle.fromDegrees(100,36,110,34),this.viewer=new Cesium.Viewer(e,t),this.resetDefault()}resetDefault(){var n,l,m,h,x;this.viewer.scene.debugShowFramesPerSecond=!0,this.viewer.scene.globe.depthTestAgainstTerrain=!0,(x=(h=(m=(l=(n=this.viewer)==null?void 0:n.homeButton)==null?void 0:l.viewModel)==null?void 0:m.command)==null?void 0:h.beforeExecute)==null||x.addEventListener(p=>{p.cancel=!0,this.viewer.camera.flyTo({destination:Cesium.Cartesian3.fromDegrees(110,32,6e6)})});const e=(p,d,f,v,C)=>new Cesium.ProviderViewModel({category:p,name:d,tooltip:f,iconUrl:v,creationFunction:()=>new Cesium.UrlTemplateImageryProvider(C)}),t=[{category:"\u9AD8\u5FB7\u5730\u56FE",name:"\u5730\u56FE\u9053\u8DEF",tooltip:"\u9AD8\u5FB7\u77E2\u91CF \u5730\u56FE\u9053\u8DEF\u670D\u52A1",iconUrl:Cesium.buildModuleUrl("Widgets/Images/ImageryProviders/openStreetMap.png"),provide:{url:"http://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}",subdomains:["1","2","3","4"],minimumLevel:3,maximumLevel:18}},{category:"\u9AD8\u5FB7\u5730\u56FE",name:"\u5730\u56FE\u74E6\u7247",tooltip:"\u9AD8\u5FB7\u77E2\u91CF \u5730\u56FE\u74E6\u7247\u670D\u52A1",iconUrl:"./map/gdwp.jpg",provide:{url:"http://wprd0{s}.is.autonavi.com/appmaptile?style=7&x={x}&y={y}&z={z}",subdomains:["1","2","3","4"],minimumLevel:3,maximumLevel:18}},{category:"\u9AD8\u5FB7\u5730\u56FE",name:"\u536B\u661F\u5730\u56FE",tooltip:"\u9AD8\u5FB7\u77E2\u91CF \u536B\u661F\u5730\u56FE\u670D\u52A1",iconUrl:"./map/gdwx.jpg",provide:{url:"http://webst0{s}.is.autonavi.com/appmaptile?style=6&x={x}&y={y}&z={z}",subdomains:["1","2","3","4"],minimumLevel:3,maximumLevel:18}}];this.viewer.baseLayerPicker.viewModel.imageryProviderViewModels.unshift(...t.map(({category:p,name:d,tooltip:f,iconUrl:v,provide:C})=>e(p,d,f,v,C)));const u=e("\u81EA\u5B9A\u4E49\u5206\u7C7B","\u8857\u666F\u5730\u56FE","openstreetmap \u8857\u666F\u5730\u56FE","./map/jiejin.jpg",{url:"https://tile.openstreetmap.org/{z}/{x}/{y}.png",subdomains:["0","1","2"]}),s=new Cesium.ProviderViewModel({category:"\u81EA\u5B9A\u4E49\u5206\u7C7B",name:"MapBox",iconUrl:"./map/mapbox.jpg",tooltip:"MapBox \u81EA\u5B9A\u4E49\u5E95\u56FE",creationFunction:()=>new Cesium.MapboxStyleImageryProvider({username:"zero-dg",styleId:"cl7fx15f6000g14p9k3sf74rc",accessToken:"pk.eyJ1IjoiemVyby1kZyIsImEiOiJjbDdmdHdsOGUwOGEyM3ZxOHJmemZ0b25uIn0.NYZEUrpIoKOvO-fIoeAZgw"})}),c=new Cesium.ProviderViewModel({category:"\u81EA\u5B9A\u4E49\u5206\u7C7B",name:"\u817E\u8BAF\u5730\u56FE",iconUrl:"./map/gdwx.jpg",tooltip:"\u817E\u8BAF\u5730\u56FE \u5F71\u50CF\u56FE",creationFunction:()=>new Cesium.UrlTemplateImageryProvider({url:"https://p2.map.gtimg.com/sateTiles/{z}/{sx}/{sy}/{x}_{reverseY}.jpg?version=400",customTags:{sx:function(p,d,f,v){return d>>4},sy:function(p,d,f,v){return(1<<v)-f>>4}},minimumLevel:3,maximumLevel:18})});this.viewer.baseLayerPicker.viewModel.imageryProviderViewModels.unshift(s,u,c),this.viewer.baseLayerPicker.viewModel.selectedImagery=s;const a=new Cesium.ProviderViewModel({category:"Cesium ion",name:"ArcGIS\u5730\u5F62",iconUrl:Cesium.buildModuleUrl("Widgets/Images/TerrainProviders/Ellipsoid.png"),tooltip:"ArcGIS\u5730\u5F62\u670D\u52A1",creationFunction:function(){return new Cesium.ArcGISTiledElevationTerrainProvider({url:"https://elevation3d.arcgis.com/arcgis/rest/services/WorldElevation3D/Terrain3D/ImageServer",token:"KED1aF_\u7533\u8BF7\u7684token.."})}});this.viewer.baseLayerPicker.viewModel.terrainProviderViewModels.push(a),document.addEventListener("keydown",p=>{p.code==="KeyF"&&(this.viewer.scene.debugShowFramesPerSecond=!this.viewer.scene.debugShowFramesPerSecond)})}rollerShutterSplitScreen(){const e=this.viewer;let t=!0;const u=this.viewer.imageryLayers.addImageryProvider(new Cesium.UrlTemplateImageryProvider({url:"http://webst0{s}.is.autonavi.com/appmaptile?style=6&x={x}&y={y}&z={z}",subdomains:["1","2","3","4"],minimumLevel:3,maximumLevel:18}));u.splitDirection=Cesium.SplitDirection.LEFT;const s=document.getElementById("split");s.style.visibility="visible",e.scene.splitPosition=s.offsetLeft/s.parentElement.offsetWidth;const c=new Cesium.ScreenSpaceEventHandler(s);let a=!1;function n(l){if(!a)return;const m=l.endPosition.x,h=(s.offsetLeft+m)/s.parentElement.offsetWidth;s.style.left=`${100*h}%`,e.scene.splitPosition=h}return c.setInputAction(function(){a=!0},Cesium.ScreenSpaceEventType.LEFT_DOWN),c.setInputAction(function(){a=!0},Cesium.ScreenSpaceEventType.PINCH_START),c.setInputAction(n,Cesium.ScreenSpaceEventType.MOUSE_MOVE),c.setInputAction(n,Cesium.ScreenSpaceEventType.PINCH_MOVE),c.setInputAction(function(){a=!1},Cesium.ScreenSpaceEventType.LEFT_UP),c.setInputAction(function(){a=!1},Cesium.ScreenSpaceEventType.PINCH_END),()=>{t?(e.scene.splitPosition=0,s.style.visibility="hidden",s.style.left=`${0}%`):(s.style.visibility="visible",e.scene.splitPosition=.5,s.style.left="50%"),t=!t}}}const i={...Cesium};class M{constructor(e={}){o(this,"_definitionChanged");o(this,"_color");o(this,"_colorSubscription");o(this,"_speed");o(this,"_speedSubscription");o(this,"color");o(this,"speed");this._definitionChanged=new i.Event,this._color=void 0,this._colorSubscription=void 0,this._speed=void 0,this._speedSubscription=void 0,this.color=e.color||i.Color.fromBytes(255,0,0,250),this.speed=e.speed||10}get isConstant(){return!1}get definitionChanged(){return this._definitionChanged}getType(e){return null}getValue(e,t){return t=i.defaultValue(t,{}),t}equals(e){return this===e}}class E extends M{constructor(e={}){super(e)}getType(e){return i.Material.EllipsoidTrailType}getValue(e,t){return i.defined(t)||(t={}),t.color=i.Property.getValueOrDefault(this._color,e,this.color,i.Color.RED),t.speed=i.Property.getValueOrDefault(this._speed,e,this.speed,10),t}equals(e){return this===e||e instanceof E&&i.Property.equals(this._color,e._color)&&i.Property.equals(this._speed,e._speed)}}Object.defineProperties(E.prototype,{color:i.createPropertyDescriptor("color"),speed:i.createPropertyDescriptor("speed")});class S extends M{constructor(e={}){super(e)}getType(e){return i.Material.EllipsoidElectricType}getValue(e,t){return i.defined(t)||(t={}),t.color=i.Property.getValueOrDefault(this._color,e,this.color,i.Color.RED),t.speed=i.Property.getValueOrDefault(this._speed,e,this.speed,10),t}equals(e){return this===e||e instanceof S&&i.Property.equals(this._color,e._color)&&i.Property.equals(this._speed,e._speed)}}Object.defineProperties(S.prototype,{color:i.createPropertyDescriptor("color"),speed:i.createPropertyDescriptor("speed")});const O={EllipsoidTrailMaterialProperty:E,EllipsoidElectricMaterialProperty:S};Object.entries(O).forEach(([r,e])=>{i[r]=e});const U=`
uniform vec4 color;
uniform float speed;
czm_material czm_getMaterial(czm_materialInput materialInput){
  czm_material material = czm_getDefaultMaterial(materialInput);
  vec2 st = materialInput.st;
  float time = fract(czm_frameNumber * speed / 1000.0);
  float alpha = abs(smoothstep(0.5,1.,fract( -st.t - time)));
  alpha += .1;
  material.alpha = alpha;
  material.diffuse = color.rgb;
  return material;
}`,j=`
uniform vec4 color;
uniform float speed;

#define pi 3.1415926535
#define PI2RAD 0.01745329252
#define TWO_PI (2. * PI)

float rands(float p){
  return fract(sin(p) * 10000.0);
}

float noise(vec2 p){
  float time = fract( czm_frameNumber * speed / 1000.0);
  float t = time / 20000.0;
  if(t > 1.0) t -= floor(t);
  return rands(p.x * 14. + p.y * sin(t) * 0.5);
}

vec2 sw(vec2 p){
  return vec2(floor(p.x), floor(p.y));
}

vec2 se(vec2 p){
  return vec2(ceil(p.x), floor(p.y));
}

vec2 nw(vec2 p){
  return vec2(floor(p.x), ceil(p.y));
}

vec2 ne(vec2 p){
  return vec2(ceil(p.x), ceil(p.y));
}

float smoothNoise(vec2 p){
  vec2 inter = smoothstep(0.0, 1.0, fract(p));
  float s = mix(noise(sw(p)), noise(se(p)), inter.x);
  float n = mix(noise(nw(p)), noise(ne(p)), inter.x);
  return mix(s, n, inter.y);
}

float fbm(vec2 p){
  float z = 2.0;
  float rz = 0.0;
  vec2 bp = p;
  for(float i = 1.0; i < 6.0; i++){
    rz += abs((smoothNoise(p) - 0.5)* 2.0) / z;
    z *= 2.0;
    p *= 2.0;
  }
  return rz;
}

czm_material czm_getMaterial(czm_materialInput materialInput)
{
  czm_material material = czm_getDefaultMaterial(materialInput);
  vec2 st = materialInput.st;
  vec2 st2 = materialInput.st;
  float time = fract( czm_frameNumber * speed / 1000.0);
  if (st.t < 0.5) {
    discard;
  }
  st *= 4.;
  float rz = fbm(st);
  st /= exp(mod( time * 2.0, pi));
  rz *= pow(15., 0.9);
  vec4 temp = vec4(0);
  temp = mix( color / rz, vec4(color.rgb, 0.1), 0.2);
  if (st2.s < 0.05) {
    temp = mix(vec4(color.rgb, 0.1), temp, st2.s / 0.05);
  }
  if (st2.s > 0.95){
    temp = mix(temp, vec4(color.rgb, 0.1), (st2.s - 0.95) / 0.05);
  }
  material.diffuse = temp.rgb;
  material.alpha = temp.a * 2.0;
  return material;
}
`;i.Material.EllipsoidTrailType="EllipsoidTrail";i.Material._materialCache.addMaterial(i.Material.EllipsoidTrailType,{fabric:{type:i.Material.EllipsoidTrailType,uniforms:{color:new i.Color(1,0,0,1),speed:5},source:U},translucent:function(r){return!0}});i.Material.EllipsoidElectricType="EllipsoidElectric";i.Material._materialCache.addMaterial(i.Material.EllipsoidElectricType,{fabric:{type:i.Material.EllipsoidTrailType,uniforms:{color:new i.Color(1,0,0,1),speed:5},source:j},translucent:function(r){return!0}});class N{constructor(e){o(this,"_viewer");o(this,"_hawkEyeMap");o(this,"_isMainMapTrigger");o(this,"_isEyeMapTrigger");o(this,"container");this._viewer=e,this._hawkEyeMap=null,this._init()}_init(){this._divInit(),this._mapInit(),this.container=this._hawkEyeMap.container}_divInit(){const e=document.createElement("div");return e.setAttribute("id","hawkEye2dMap"),e.style.cssText=`
      position: absolute;
      left: 10px;
      top: 10px;
      border-radius: 50%;
      height: 160px;
      width: 160px;
      overflow: hidden;
      border: 2px solid #002FA7;`,this._viewer._container.appendChild(e),e}_mapInit(){this._hawkEyeMap=new i.Viewer("hawkEye2dMap",{geocoder:!1,homeButton:!1,sceneModePicker:!1,baseLayerPicker:!1,navigationHelpButton:!1,animation:!1,timeline:!1,fullscreenButton:!1,sceneMode:i.SceneMode.SCENE2D}),this._hawkEyeMap.cesiumWidget.creditContainer.style.display="none",this._hawkEyeMap.scene.backgroundColor=i.Color.TRANSPARENT,this._hawkEyeMap.imageryLayers.removeAll(),this._hawkEyeMap.imageryLayers.addImageryProvider(new i.UrlTemplateImageryProvider({url:"http://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}",subdomains:["1","2","3","4"],minimumLevel:3,maximumLevel:18})),this._viewer.camera.percentageChanged=.02,this._hawkEyeMap.camera.percentageChanged=.5,this._bindEvent()}_bindEvent(){this._viewer.camera.changed.addEventListener(this._syncEyeMap,this),this._viewer.scene.preRender.addEventListener(this._syncEyeMap,this)}_syncEyeMap(){new i.ScreenSpaceEventHandler(this._viewer.canvas).setInputAction(()=>{this._isMainMapTrigger=!0,this._isEyeMapTrigger=!1},i.ScreenSpaceEventType.MOUSE_MOVE);const e=new i.Cartesian2(Math.floor(this._viewer.canvas.clientWidth/2),Math.floor(this._viewer.canvas.clientHeight/2)),t=this._viewer.scene.camera.pickEllipsoid(e);if(!t)return!1;const u=i.Cartesian3.distance(t,this._viewer.scene.camera.positionWC);this._hawkEyeMap.scene.camera.lookAt(t,new i.Cartesian3(0,0,u))}}const R=(r,e="IsCesium-pic")=>{function t(){r.render();const c=r.scene.canvas.toDataURL("image/png").replace("image/png","image/octet-stream"),a=document.createElement("a"),n=u(c),l=URL.createObjectURL(n);a.download=`${e}.png`,a.href=l,a.click()}function u(s){const c=s.split(","),a=c[0].match(/:(.*?);/)[1],n=atob(c[1]);let l=n.length;const m=new Uint8Array(l);for(;l--;)m[l]=n.charCodeAt(l);return new Blob([m],{type:a})}t()},H={HawkEye2DMap:N,SaveImage:R};i.Tools={};Object.entries(H).forEach(([r,e])=>{i.Tools[r]=e});class W{constructor(e,t){o(this,"snowSize");o(this,"snowSpeed");o(this,"viewer");o(this,"snowStage");if(!e)throw new Error("no viewer object!");t=t||{},this.snowSize=i.defaultValue(t.snowSize,.02),this.snowSpeed=i.defaultValue(t.snowSpeed,60),this.viewer=e}init(){this.snowStage=new i.PostProcessStage({name:"czm_snow",fragmentShader:this.snow(),uniforms:{snowSize:()=>this.snowSize,snowSpeed:()=>this.snowSpeed}}),this.viewer.scene.postProcessStages.add(this.snowStage)}destroy(){!this.viewer||!this.snowStage||this.viewer.scene.postProcessStages.remove(this.snowStage)}show(e){this.snowStage.enabled=e}optShow(){var e,t;(t=(e=this.snowStage)==null?void 0:e.isDestroyed())==null||t?this.init():this.destroy()}snow(){return`
      uniform sampler2D colorTexture;
      varying vec2 v_textureCoordinates;
      uniform float snowSpeed;
      uniform float snowSize;

      float snow(vec2 uv,float scale){
          float time=czm_frameNumber/snowSpeed;
          float w=smoothstep(1.,0.,-uv.y*(scale/10.));if(w<.1)return 0.;
          uv+=time/scale;
					uv.y+=time*2./scale;
					uv.x+=sin(uv.y+time*.5)/scale;
          uv*=scale;vec2 s=floor(uv),f=fract(uv),p;float k=3.,d;
          p=.5+.35*sin(11.*fract(sin((s+p+scale)*mat2(7,3,6,5))*5.))-f;d=length(p);k=min(d,k);
          k=smoothstep(0.,k,sin(f.x+f.y)*snowSize);
          return k*w;
      }

      void main(void){
          vec2 resolution=czm_viewport.zw;
          vec2 uv=(gl_FragCoord.xy*2.-resolution.xy)/min(resolution.x,resolution.y);
          vec3 finalColor=vec3(0);
          //float c=smoothstep(1.,0.3,clamp(uv.y*.3+.8,0.,.75));
          float c=0.;
          c+=snow(uv,30.)*.0;
          c+=snow(uv,20.)*.0;
          c+=snow(uv,15.)*.0;
          c+=snow(uv,10.);
          c+=snow(uv,8.);
          c+=snow(uv,6.);
          c+=snow(uv,5.);
          finalColor=(vec3(c));
          gl_FragColor=mix(texture2D(colorTexture,v_textureCoordinates),vec4(finalColor,1),.5);
      }
    `}}class q{constructor(e,t){o(this,"tiltAngle");o(this,"rainSize");o(this,"rainSpeed");o(this,"viewer");o(this,"rainStage");if(!e)throw new Error("no viewer object!");t=t||{},this.tiltAngle=i.defaultValue(t.tiltAngle,-.6),this.rainSize=i.defaultValue(t.rainSize,.3),this.rainSpeed=i.defaultValue(t.rainSpeed,60),this.viewer=e}init(){this.rainStage=new i.PostProcessStage({name:"czm_rain",fragmentShader:this.rain(),uniforms:{tiltAngle:()=>this.tiltAngle,rainSize:()=>this.rainSize,rainSpeed:()=>this.rainSpeed}}),this.viewer.scene.postProcessStages.add(this.rainStage)}destroy(){!this.viewer||!this.rainStage||this.viewer.scene.postProcessStages.remove(this.rainStage)}show(e){this.rainStage.enabled=e}optShow(){var e,t;(t=(e=this.rainStage)==null?void 0:e.isDestroyed())==null||t?this.init():this.destroy()}rain(){return`
      uniform sampler2D colorTexture;
      varying vec2 v_textureCoordinates;
      uniform float tiltAngle;
      uniform float rainSize;
      uniform float rainSpeed;

      float hash(float x) {
          return fract(sin(x * 133.3) * 13.13);
      }

      void main(void) {
          float time = czm_frameNumber / rainSpeed;
          vec2 resolution = czm_viewport.zw;
          vec2 uv = (gl_FragCoord.xy * 2. - resolution.xy) / min(resolution.x, resolution.y);
          vec3 c = vec3(.6, .7, .8);
          float a = tiltAngle;
          float si = sin(a), co = cos(a);
          uv *= mat2(co, -si, si, co);
          uv *= length(uv + vec2(0, 4.9)) * rainSize + 1.;
          float v = 1. - sin(hash(floor(uv.x * 100.)) * 2.);
          float b = clamp(abs(sin(20. * time * v + uv.y * (5. / (2. + v)))) - .95, 0., 1.) * 20.;
          c *= v * b;
          gl_FragColor = mix(texture2D(colorTexture, v_textureCoordinates), vec4(c, 1), .5);
      }
    `}}class G{constructor(e,t){o(this,"visibility");o(this,"color");o(this,"viewer");o(this,"fogStage");if(!e)throw new Error("no viewer object!");t=t||{},this.visibility=i.defaultValue(t.visibility,.1),this.color=i.defaultValue(t.color,new i.Color(.8,.8,.8,.5)),this.viewer=e}init(){this.fogStage=new i.PostProcessStage({name:"czm_fog",fragmentShader:this.fog(),uniforms:{visibility:()=>this.visibility,fogColor:()=>this.color}}),this.viewer.scene.postProcessStages.add(this.fogStage)}destroy(){!this.viewer||!this.fogStage||this.viewer.scene.postProcessStages.remove(this.fogStage)}show(e){this.fogStage.enabled=e}optShow(){var e,t;(t=(e=this.fogStage)==null?void 0:e.isDestroyed())==null||t?this.init():this.destroy()}fog(){return`
      uniform sampler2D colorTexture;
      uniform sampler2D depthTexture;
      uniform float visibility;
      uniform vec4 fogColor;
      varying vec2 v_textureCoordinates; 
      void main(void) 
      { 
         vec4 origcolor = texture2D(colorTexture, v_textureCoordinates); 
         float depth = czm_readDepth(depthTexture, v_textureCoordinates); 
         vec4 depthcolor = texture2D(depthTexture, v_textureCoordinates); 
         float f = visibility * (depthcolor.r - 0.3) / 0.2; 
         if (f < 0.0) f = 0.0; 
         else if (f > 1.0) f = 1.0; 
         gl_FragColor = mix(origcolor, fogColor, f); 
      }
    `}}const Z={SnowEffect:W,RainEffect:q,FogEffect:G};i.Effect={};Object.entries(Z).forEach(([r,e])=>{i.Effect[r]=e});function J(r){r.entities.add({position:Cesium.Cartesian3.fromDegrees(110,35),name:"\u8F68\u8FF9\u7403\u4F53",ellipsoid:{radii:new Cesium.Cartesian3(2e5,2e5,2e5),material:new i.EllipsoidTrailMaterialProperty({color:Cesium.Color.fromCssColorString("#00ff04"),speed:15}),outline:!0,outlineColor:new Cesium.Color(0,1,0,1)}}),r.entities.add({position:Cesium.Cartesian3.fromDegrees(110,31),name:"\u7535\u5F27\u7403\u4F53",ellipsoid:{radii:new Cesium.Cartesian3(2e5,2e5,2e5),material:new i.EllipsoidElectricMaterialProperty({color:new Cesium.Color(1,1,0,1),speed:10})}});const e=new i.Tools.HawkEye2DMap(r);e.container.style.cssText=`
		position: absolute;
		left: 10px;
		top: 10px;
		height: 200px;
		width: 260px;
		overflow: hidden;
	`}const K=i.Tools.SaveImage,$=r=>{const e=new i.Effect.SnowEffect(r,{snowSize:.02,snowSpeed:100});return()=>{e.optShow()}},Y=r=>{const e=new i.Effect.RainEffect(r,{tiltAngle:-.2,rainSize:.6,rainSpeed:100});return()=>{e.optShow()}},Q=r=>{const e=new i.Effect.FogEffect(r,{visibility:.2,color:new Cesium.Color(.8,.8,.8,.3)});return()=>{e.optShow()}},X=r=>(k("data-v-2ce3455e"),r=r(),A(),r),ee={id:"cesium-box"},te={id:"operation-box"},ie=_("\u5BFC\u51FApng"),re=_("\u5377\u5E18\u5F00\u5173"),oe=_("\u96EA\u82B1\u6548\u679C"),se=_("\u4E0B\u96E8\u6548\u679C"),ae=_("\u96FE\u5929\u6548\u679C"),ne=X(()=>b("div",{id:"split"},null,-1)),le=F({__name:"CesiumView",setup(r){const e=g(),t=g(),u=g(),s=g(),c=g();return z(()=>{const a=new V("cesium-box"),{viewer:n}=a;J(n),e.value=()=>{K(n,"toPng")},t.value=a.rollerShutterSplitScreen(),t.value(),u.value=$(n),s.value=Y(n),c.value=Q(n)}),(a,n)=>{const l=L;return I(),D("div",ee,[b("div",te,[w(l,{type:"primary",onClick:e.value},{default:y(()=>[ie]),_:1},8,["onClick"]),w(l,{type:"primary",onClick:t.value},{default:y(()=>[re]),_:1},8,["onClick"]),w(l,{type:"primary",onClick:u.value},{default:y(()=>[oe]),_:1},8,["onClick"]),w(l,{type:"primary",onClick:s.value},{default:y(()=>[se]),_:1},8,["onClick"]),w(l,{type:"primary",onClick:c.value},{default:y(()=>[ae]),_:1},8,["onClick"])]),ne])}}});const de=B(le,[["__scopeId","data-v-2ce3455e"]]);export{de as default};
