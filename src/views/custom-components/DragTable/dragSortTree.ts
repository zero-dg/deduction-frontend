// 表格拖拽排序
const lightLine = document.createElement('div');
lightLine.style.cssText += ';height: 1px; position: absolute; top: 0; left: 0; right: 0; background-color: skyblue; z-index: 999; display: none';
export function useTableDragSort() {
	let callback: (data: any[]) => void;
	function initDragSort(tableBodyDom: HTMLElement, tableData: any[]) {
		setTimeout(() => {
			const trList = tableBodyDom.querySelectorAll<HTMLTableRowElement>('tr');
			const deviationTop = document.getElementsByClassName('el-table__body-wrapper')[0].children[0].children[0]; //过长内容数据被卷去的高度
			tableBodyDom.appendChild(lightLine);
			// 一维数据
			const flatData = getChildrenItem(tableData, []);

			// 当前拖拽对象
			let currentDragDom: HTMLTableRowElement;
			let currentDragIndex: number;
			for (let i = 0; i < trList.length; i++) {
				trList[i].onmousedown = () => {
					trList[i].setAttribute('draggable', 'true');
					currentDragDom = trList[i];
					currentDragIndex = i;
				};
				trList[i].onmouseup = () => {
					trList[i].removeAttribute('draggable');
				};
				// 进入tr，展示高亮线
				trList[i].ondragover = (e: DragEvent) => {
					e.preventDefault();
					if (isDragInChildNode(flatData[currentDragIndex], flatData[i])) {
						lightLine.style.cssText += ';display: none';
						return;
					}
					const trHeight = trList[i].clientHeight;
					// 所在行的偏移百分比
					const curPercentage = Math.abs(e.offsetY / trHeight);
					// 0 0.5 1 前（插入子）后
					const enterPosition = curPercentage < 1 / 4 ? 0 : curPercentage < 3 / 4 ? 0.5 : 1;
					// 偏移像素 三段化
					const top = Math.max(enterPosition * trHeight + trList[i].offsetTop - deviationTop.scrollTop, 0);
					lightLine.style.cssText += `;left: 0px; top: ${top}px; display: block`;
				};
				trList[i].ondrop = (e: DragEvent) => {
					currentDragDom.removeAttribute('draggable');
					if (isDragInChildNode(flatData[currentDragIndex], flatData[i])) return;
					const trHeight = trList[i].clientHeight;
					// 所在行的偏移百分比
					const curPercentage = Math.abs(e.offsetY / trHeight);
					// 0 0.5 1 前（插入子）后
					const enterPosition = curPercentage < 1 / 4 ? 0 : curPercentage < 3 / 4 ? 0.5 : 1;
					// 0 在前面插入 1 插入子节点 2 在后面插入
					const positionValue = enterPosition * 2;
					const newTableData = changeData(tableData, flatData[currentDragIndex], flatData[i], positionValue);
					newTableData && callback(newTableData);
				};
				trList[i].ondragend = () => {
					lightLine.style.cssText += ';display: none';
				};
			}
		}, 0);
	}
	// 获取所有子项处理为一维数组
	function getChildrenItem(arr: any[], res: any[]) {
		if (!arr) return res;
		arr.forEach((checkItem) => {
			res.push({
				...checkItem,
			});
			getChildrenItem(checkItem.children, res);
		});
		return res;
	}
	// 判断父节点是否拖入其子节点中
	function isDragInChildNode(dragData: any, enterData: any) {
		if (dragData.id === enterData.id) return true;
		if (!dragData.children) return false;
		const children = JSON.parse(JSON.stringify(dragData.children));
		while (children.length > 0) {
			const cur = children.pop();
			if (cur.id === enterData.id) {
				return true;
			} else if (cur.children) {
				children.push(...cur.children);
			}
		}
		return false;
	}
	// 处理拖拽排序前后的数据
	function changeData(parentData: any[], dragData: any, enterData: any, positionValue: number) {
		const res = JSON.parse(JSON.stringify(parentData));
		const stack = [res];
		let addOk = false;
		let removeOk = false;
		while (stack.length > 0) {
			const cur = stack.pop() || [];
			const ids = cur.map((item: any) => item.id);
			const dragIndex = ids.indexOf(dragData.id);
			const enterIndex = ids.indexOf(enterData.id);
			// 同级情况 前前 后后 不变化
			if (dragIndex >= 0 && enterIndex >= 0) {
				if (dragIndex - enterIndex === -1 && positionValue === 0) return;
				if (dragIndex - enterIndex === 1 && positionValue === 2) return;
			}
			if (dragIndex >= 0 && !removeOk) {
				cur.splice(dragIndex, 1);
				removeOk = true;
			}
			if (enterIndex >= 0 && !addOk) {
				addOk = true;
				// 同级判断 对接行(enterIndex)前后， 行前影响enterIndex
				if (dragIndex >= 0 && dragIndex < enterIndex) {
					if (positionValue === 0) {
						cur.splice(enterIndex - 1, 0, dragData);
					} else if (positionValue === 2) {
						cur.splice(enterIndex, 0, dragData);
					} else {
						if (cur[enterIndex - 1]?.children?.length) {
							cur[enterIndex - 1].children.push(dragData);
						} else {
							cur[enterIndex - 1]['children'] = [dragData];
						}
					}
				} else {
					if (positionValue === 0) {
						cur.splice(enterIndex, 0, dragData);
					} else if (positionValue === 2) {
						cur.splice(enterIndex + 1, 0, dragData);
					} else {
						if (cur[enterIndex]?.children?.length) {
							cur[enterIndex].children.push(dragData);
						} else {
							cur[enterIndex]['children'] = [dragData];
						}
					}
				}
			}
			if (!addOk || !removeOk) {
				cur.forEach((item: any) => stack.push(item.children));
			} else {
				return res;
			}
		}
	}
	function dragEnd(cb: (data: any[]) => void) {
		callback = cb;
	}
	return {
		initDragSort,
		dragEnd,
	};
}
