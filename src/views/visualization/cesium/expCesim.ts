import * as Cesium from 'cesium';

export default class isCesium {
	// 地图实体实例
	viewer: Cesium.Viewer;

	constructor(
		container: HTMLElement | string,
		options: Cesium.Viewer.ConstructorOptions = {
			animation: true, // 控制场景动画的播放速度控件 左下角小控件
			baseLayerPicker: true, // 底图切换控件
			fullscreenButton: false, // 全屏控件
			geocoder: false, // 地理位置查询定位控件
			homeButton: true, // 默认相机位置控件
			timeline: true, // 时间滚动条控件
			sceneModePicker: true, //是否显示3D/2D选择器
			sceneMode: Cesium.SceneMode.SCENE3D, //设定3维地图的默认场景模式:Cesium.SceneMode.SCENE2D、Cesium.SceneMode.SCENE3D、Cesium.SceneMode.MORPHING
			navigationHelpButton: false, // 默认的相机控制提示控件  右上角提示信息
			// terrainProvider: Cesium.createWorldTerrain(), //初始化加载高程数据
			selectionIndicator: true, //绿色框--相机选中
			// vrButton: true, // VR模式
			// scene3DOnly: true, // 每个几何实例仅以3D渲染以节省GPU内存  开启后不能3D和2D切换
			// baselLayerPicker: false // 将图层选择的控件关掉，才能添加其他影像数据
			infoBox: false, //是否显示信息框
			// navigationInstructionsInitiallyVisible: true,
			// showRenderLoopErrors: false, //是否显示渲染错误
			orderIndependentTranslucency: true, //设置背景透明
			// msaaSamples: 4, // 多重采样抗锯齿（MSAA） 抗锯齿效果
		}
	) {
		// 修改相机初始化默认范围
		Cesium.Camera.DEFAULT_VIEW_RECTANGLE = Cesium.Rectangle.fromDegrees(100, 36, 110, 34);
		this.viewer = new Cesium.Viewer(container, options);
		this.resetDefault();
	}

	/**
	 * 修改默认配置 重写方法
	 * https://zhuanlan.zhihu.com/p/350724716
	 * https://github.com/ls870061011/cesium_training/blob/main/examples/3_1_3baseLayerPicker.html
	 */
	resetDefault() {
		// 显示帧率
		this.viewer.scene.debugShowFramesPerSecond = true;
		// 开启地形深度检测
		this.viewer.scene.globe.depthTestAgainstTerrain = true;
		//取消双击追踪事件
		// this.viewer.cesiumWidget.screenSpaceEventHandler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);
		//左键点击 默认会选中point点需要禁止这个事件
		// this.viewer.cesiumWidget.screenSpaceEventHandler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK);

		/* 默认相机位置 配合homeButton使用 */
		this.viewer?.homeButton?.viewModel?.command?.beforeExecute?.addEventListener((e: any) => {
			e.cancel = true;
			// 飞到中国
			this.viewer.camera.flyTo({
				destination: Cesium.Cartesian3.fromDegrees(110, 32, 6000000),
			});
		});

		/* 自定义影像图层 https://blog.csdn.net/weixin_45728126/article/details/119040004 */
		const customMapCreate = (category: string, name: string, tooltip: string, iconUrl: string, provide: { url: string; subdomains: string[] }) => {
			return new Cesium.ProviderViewModel({
				category,
				name,
				tooltip,
				iconUrl,
				creationFunction: () => {
					return new Cesium.UrlTemplateImageryProvider(provide);
				},
			});
		};
		// 高德地图
		const gaodeMapList = [
			{
				category: '高德地图',
				name: '地图道路',
				tooltip: '高德矢量 地图道路服务',
				iconUrl: Cesium.buildModuleUrl('Widgets/Images/ImageryProviders/openStreetMap.png'),
				provide: {
					url: 'http://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}',
					subdomains: ['1', '2', '3', '4'],
					minimumLevel: 3,
					maximumLevel: 18,
				},
			},
			{
				category: '高德地图',
				name: '地图瓦片',
				tooltip: '高德矢量 地图瓦片服务',
				iconUrl: './map/gdwp.jpg',
				provide: {
					url: 'http://wprd0{s}.is.autonavi.com/appmaptile?style=7&x={x}&y={y}&z={z}',
					subdomains: ['1', '2', '3', '4'],
					minimumLevel: 3,
					maximumLevel: 18,
				},
			},
			{
				category: '高德地图',
				name: '卫星地图',
				tooltip: '高德矢量 卫星地图服务',
				iconUrl: './map/gdwx.jpg',
				provide: {
					url: 'http://webst0{s}.is.autonavi.com/appmaptile?style=6&x={x}&y={y}&z={z}',
					subdomains: ['1', '2', '3', '4'],
					minimumLevel: 3,
					maximumLevel: 18,
				},
			},
		];
		// 添加高德地图系列
		this.viewer.baseLayerPicker.viewModel.imageryProviderViewModels.unshift(
			...gaodeMapList.map(({ category, name, tooltip, iconUrl, provide }) => {
				return customMapCreate(category, name, tooltip, iconUrl, provide);
			})
		);
		// 街景地图
		const openstreetmapVM = customMapCreate('自定义分类', '街景地图', 'openstreetmap 街景地图', './map/jiejin.jpg', {
			url: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
			subdomains: ['0', '1', '2'],
		});
		// mapbox
		const mapBoxVM = new Cesium.ProviderViewModel({
			category: '自定义分类',
			name: `MapBox`,
			iconUrl: './map/mapbox.jpg',
			tooltip: 'MapBox 自定义底图',
			creationFunction: () => {
				return new Cesium.MapboxStyleImageryProvider({
					username: 'zero-dg',
					styleId: 'cl7fx15f6000g14p9k3sf74rc',
					accessToken: 'pk.eyJ1IjoiemVyby1kZyIsImEiOiJjbDdmdHdsOGUwOGEyM3ZxOHJmemZ0b25uIn0.NYZEUrpIoKOvO-fIoeAZgw',
				});
			},
		});
		// 腾讯在线地图
		const tenxunVM = new Cesium.ProviderViewModel({
			category: '自定义分类',
			name: `腾讯地图`,
			iconUrl: './map/gdwx.jpg',
			tooltip: '腾讯地图 影像图',
			creationFunction: () => {
				return new Cesium.UrlTemplateImageryProvider({
					url: 'https://p2.map.gtimg.com/sateTiles/{z}/{sx}/{sy}/{x}_{reverseY}.jpg?version=400',
					customTags: {
						sx: function (imageryProvider: any, x: number, y: any, level: any) {
							return x >> 4;
						},
						sy: function (imageryProvider: any, x: any, y: number, level: number) {
							return ((1 << level) - y) >> 4;
						},
					},
					minimumLevel: 3,
					maximumLevel: 18,
					// 矢量图
					// url: "https://rt3.map.gtimg.com/tile?z={z}&x={x}&y={reverseY}&styleid=1&version=297",
					// 黑色风格
					// url: "https://rt3.map.gtimg.com/tile?z={z}&x={x}&y={reverseY}&styleid=4&scene=0",
					// 注记图1
					// url: "https://rt3.map.gtimg.com/tile?z={z}&x={x}&y={reverseY}&styleid=3&scene=0",
					// 注记图2
					// url: "https://rt3.map.gtimg.com/tile?z={z}&x={x}&y={reverseY}&styleid=2&version=297",
				});
			},
		});
		// 自定义添加
		this.viewer.baseLayerPicker.viewModel.imageryProviderViewModels.unshift(mapBoxVM, openstreetmapVM, tenxunVM);

		// 设置默认选中的底图
		this.viewer.baseLayerPicker.viewModel.selectedImagery = mapBoxVM;

		/* 自定义地形图层 */
		const arcgisVM = new Cesium.ProviderViewModel({
			category: 'Cesium ion',
			name: 'ArcGIS地形',
			iconUrl: Cesium.buildModuleUrl('Widgets/Images/TerrainProviders/Ellipsoid.png'),
			tooltip: 'ArcGIS地形服务',
			creationFunction: function () {
				return new Cesium.ArcGISTiledElevationTerrainProvider({
					url: 'https://elevation3d.arcgis.com/arcgis/rest/services/WorldElevation3D/Terrain3D/ImageServer',
					token: 'KED1aF_申请的token..',
				});
			},
		});
		this.viewer.baseLayerPicker.viewModel.terrainProviderViewModels.push(arcgisVM);

		/* 键盘事件监听 */
		document.addEventListener('keydown', (e) => {
			if (e.code === 'KeyF') {
				// 显示帧率控制
				this.viewer.scene.debugShowFramesPerSecond = !this.viewer.scene.debugShowFramesPerSecond;
			}
		});
	}

	/**
	 * 卷帘分屏
	 */
	rollerShutterSplitScreen() {
		const _viewer = this.viewer;
		let enable = true;
		// 添加右半侧地图
		const layerLeft = this.viewer.imageryLayers.addImageryProvider(
			new Cesium.UrlTemplateImageryProvider({
				url: 'http://webst0{s}.is.autonavi.com/appmaptile?style=6&x={x}&y={y}&z={z}',
				subdomains: ['1', '2', '3', '4'],
				minimumLevel: 3,
				maximumLevel: 18,
			})
		);
		// 设置分屏显示
		layerLeft.splitDirection = Cesium.SplitDirection.LEFT;

		// 设置分屏位置
		const split: any = document.getElementById('split');
		split.style.visibility = 'visible';
		_viewer.scene.splitPosition = split.offsetLeft / split.parentElement.offsetWidth;

		const handler = new Cesium.ScreenSpaceEventHandler(split);

		let moveActive = false;

		function move(movement: { endPosition: { x: any } }) {
			if (!moveActive) {
				return;
			}

			const relativeOffset = movement.endPosition.x;
			const splitPosition = (split.offsetLeft + relativeOffset) / split.parentElement.offsetWidth;
			split.style.left = `${100.0 * splitPosition}%`;
			_viewer.scene.splitPosition = splitPosition;
		}

		handler.setInputAction(function () {
			moveActive = true;
		}, Cesium.ScreenSpaceEventType.LEFT_DOWN);
		handler.setInputAction(function () {
			moveActive = true;
		}, Cesium.ScreenSpaceEventType.PINCH_START);

		handler.setInputAction(move, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
		handler.setInputAction(move, Cesium.ScreenSpaceEventType.PINCH_MOVE);

		handler.setInputAction(function () {
			moveActive = false;
		}, Cesium.ScreenSpaceEventType.LEFT_UP);
		handler.setInputAction(function () {
			moveActive = false;
		}, Cesium.ScreenSpaceEventType.PINCH_END);

		// 开关控制
		return () => {
			if (enable) {
				_viewer.scene.splitPosition = 0;
				split.style.visibility = 'hidden';
				split.style.left = `${0}%`;
			} else {
				split.style.visibility = 'visible';
				_viewer.scene.splitPosition = 0.5;
				split.style.left = `50%`;
			}
			enable = !enable;
		};
	}
}
