import * as Cesium from 'cesium';
import IsCesium from '@/plugins/IsCesium';
import CD from './cd.geojson?url';

// 绘制一些默认实体
export function drawDemoTest(viewer: Cesium.Viewer) {
	// 轨迹球体
	viewer.entities.add({
		position: Cesium.Cartesian3.fromDegrees(103.9, 30.5),
		name: '轨迹球体',
		ellipsoid: {
			radii: new Cesium.Cartesian3(10000, 10000, 10000),
			material: new IsCesium.EllipsoidTrailMaterialProperty({
				color: Cesium.Color.fromCssColorString('#00ff04'),
				speed: 15,
			}),
			// innerRadii: new Cesium.Cartesian3((200000 * 10) / 90, (200000 * 10) / 90, 30),
			// minimumCone: Cesium.Math.toRadians(60.0),
			// maximumCone: Cesium.Math.toRadians(90.0),
			outline: true,
			outlineColor: new Cesium.Color(0, 1, 0, 1),
		},
	});

	// 电弧球体效果
	viewer.entities.add({
		position: Cesium.Cartesian3.fromDegrees(104.2, 30.8),
		name: '电弧球体',
		ellipsoid: {
			radii: new Cesium.Cartesian3(10000, 10000, 10000),
			material: new IsCesium.EllipsoidElectricMaterialProperty({
				color: new Cesium.Color(1.0, 1.0, 0.0, 1.0),
				speed: 10,
			}),
		},
	});

	// 流光线
	const polyline: any = {
		polyline: {
			positions: Cesium.Cartesian3.fromDegreesArray([103.9, 30.5, 104.2, 30.5, 104.2, 30.8, 103.9, 30.8, 103.9, 30.5]),
			width: 20,
			// material: Cesium.Color.RED,
			/* material: new IsCesium.PolylineLightingTrailMaterialProperty({
				color: new Cesium.Color(1.0, 0.0, 0.0, 1.0),
				speed: 5,
			}), */
			material: new IsCesium.PolylineImageTrailMaterialProperty({
				color: new Cesium.Color(1.0, 1.0, 0.0, 1.0),
				image: IsCesium.ASSETS.ARROW_FINE,
				speed: 10,
				repeat: { x: 60, y: 1 },
			}),
			clampToGround: true,
		},
	};
	viewer.entities.add(polyline);

	// 加载成都出租车轨迹线路图  精灵线
	Cesium.GeoJsonDataSource.load(CD).then(function (dataSource) {
		const imagesMap = [undefined, IsCesium.ASSETS.SPRITE_BRIGHT_ORANGE, IsCesium.ASSETS.SPRITE_YELLOW_GREEN];
		viewer.dataSources.add(dataSource);
		const entities: any = dataSource.entities.values;
		for (let i = 0; i < entities.length; i++) {
			const image = imagesMap[i % 3];
			const speed = (Math.random() + 0.5) * 10000;
			const entity: any = entities[i];
			entity.polyline.width = 1;
			entity.polyline.clampToGround = true;
			entity.polyline.material = new IsCesium.PolylineSpriteTrailMaterialProperty({
				color: new Cesium.Color(1.0, 1.0, 0.0, 1.0),
				speed,
				image,
			});
		}
	});

	// 2D鹰眼小地图
	const hawkEye2dMap = new IsCesium.Tools.HawkEye2DMap(viewer);
	// container就是窗口dom实体，可以修改位置，默认是鹰眼图，这里做一个修改示例
	hawkEye2dMap.container.style.cssText = `
		position: absolute;
		left: 10px;
		top: 10px;
		height: 200px;
		width: 260px;
		overflow: hidden;
	`;
}

// 导出图片
export const exportImage = IsCesium.Tools.SaveImage;

// 雪花效果开关
export const enableSnowEffect = (viewer: Cesium.Viewer) => {
	const snowInstance = new IsCesium.Effect.SnowEffect(viewer, {
		snowSize: 0.02, // 雪花大小
		snowSpeed: 100.0, // 雪速
	});
	return () => {
		snowInstance.optShow();
	};
};

// 雨天效果开关
export const enableRainEffect = (viewer: Cesium.Viewer) => {
	const rainInstance = new IsCesium.Effect.RainEffect(viewer, {
		tiltAngle: -0.2, //倾斜角度
		rainSize: 0.6, // 雨大小
		rainSpeed: 100.0, // 雨速
	});
	return () => {
		rainInstance.optShow();
	};
};

// 雾气效果
export const enableFogEffect = (viewer: Cesium.Viewer) => {
	const fogInstance = new IsCesium.Effect.FogEffect(viewer, {
		visibility: 0.2,
		color: new Cesium.Color(0.8, 0.8, 0.8, 0.3),
	});
	return () => {
		fogInstance.optShow();
	};
};
