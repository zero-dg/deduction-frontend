import IsCesium from '@/plugins/IsCesium/IsCesium';
import LIGHTING from './images/lighting.png';
import SPRITE_BLUE from './images/spriteblue.png';
import SPRITE_YELLOW_GREEN from './images/spriteYellowGreen.png';
import SPRITE_BRIGHT_ORANGE from './images/spritelineBrightOrange.png';
import ARROW_CURDE from './images/arrowCrude.png';
import ARROW_FINE from './images/arrowFine.png';

const imgList = {
	LIGHTING,
	SPRITE_BLUE,
	SPRITE_YELLOW_GREEN,
	SPRITE_BRIGHT_ORANGE,
	ARROW_CURDE,
	ARROW_FINE,
};

// 资源类
IsCesium['ASSETS'] = {};

Object.entries(imgList).forEach(([key, value]: any) => {
	IsCesium.ASSETS[key] = value;
});
