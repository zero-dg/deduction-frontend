import IsCesium from '../IsCesium';

/**
 * 雾气效果
 */
export default class FogEffect {
	visibility: any;
	color: any;
	viewer: any;
	fogStage: any;

	constructor(viewer: any, options: { visibility?: any; color?: any }) {
		if (!viewer) throw new Error('no viewer object!');
		options = options || {};
		this.visibility = IsCesium.defaultValue(options.visibility, 0.1);
		this.color = IsCesium.defaultValue(options.color, new IsCesium.Color(0.8, 0.8, 0.8, 0.5));
		this.viewer = viewer;
	}

	init() {
		this.fogStage = new IsCesium.PostProcessStage({
			name: 'czm_fog',
			fragmentShader: this.fog(),
			uniforms: {
				visibility: () => {
					return this.visibility;
				},
				fogColor: () => {
					return this.color;
				},
			},
		});
		this.viewer.scene.postProcessStages.add(this.fogStage);
	}

	destroy() {
		if (!this.viewer || !this.fogStage) return;
		this.viewer.scene.postProcessStages.remove(this.fogStage);
	}

	show(visible: any) {
		this.fogStage.enabled = visible;
	}

	// 优化显示隐藏方式，减少内存占用
	optShow() {
		// undefined 未定义 true 已销毁 false 未销毁
		if (this.fogStage?.isDestroyed() ?? '1' === '1') {
			this.init();
		} else {
			this.destroy();
		}
	}

	fog() {
		return `
      uniform sampler2D colorTexture;
      uniform sampler2D depthTexture;
      uniform float visibility;
      uniform vec4 fogColor;
      varying vec2 v_textureCoordinates; 
      void main(void) 
      { 
         vec4 origcolor = texture2D(colorTexture, v_textureCoordinates); 
         float depth = czm_readDepth(depthTexture, v_textureCoordinates); 
         vec4 depthcolor = texture2D(depthTexture, v_textureCoordinates); 
         float f = visibility * (depthcolor.r - 0.3) / 0.2; 
         if (f < 0.0) f = 0.0; 
         else if (f > 1.0) f = 1.0; 
         gl_FragColor = mix(origcolor, fogColor, f); 
      }
    `;
	}
}
