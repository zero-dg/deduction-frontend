import IsCesium from '../IsCesium';
import SnowEffect from './SnowEffect';
import RainEffect from './RainEffect';
import FogEffect from './FogEffect';

const effectList = {
	SnowEffect,
	RainEffect,
	FogEffect,
};

// 效果相关
IsCesium['Effect'] = {};

Object.entries(effectList).forEach(([key, value]: any) => {
	IsCesium.Effect[key] = value;
});
