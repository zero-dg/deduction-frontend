import IsCesium from '../IsCesium';

/**
 * 下雨效果
 */
export default class RainEffect {
	tiltAngle: any;
	rainSize: any;
	rainSpeed: any;
	viewer: any;
	rainStage: any;

	constructor(viewer: any, options: { tiltAngle?: any; rainSize?: any; rainSpeed?: any }) {
		if (!viewer) throw new Error('no viewer object!');
		options = options || {};
		//倾斜角度，负数向右，正数向左
		this.tiltAngle = IsCesium.defaultValue(options.tiltAngle, -0.6);
		this.rainSize = IsCesium.defaultValue(options.rainSize, 0.3);
		this.rainSpeed = IsCesium.defaultValue(options.rainSpeed, 60.0);
		this.viewer = viewer;
	}

	init() {
		this.rainStage = new IsCesium.PostProcessStage({
			name: 'czm_rain',
			fragmentShader: this.rain(),
			uniforms: {
				tiltAngle: () => {
					return this.tiltAngle;
				},
				rainSize: () => {
					return this.rainSize;
				},
				rainSpeed: () => {
					return this.rainSpeed;
				},
			},
		});
		this.viewer.scene.postProcessStages.add(this.rainStage);
	}

	destroy() {
		if (!this.viewer || !this.rainStage) return;
		this.viewer.scene.postProcessStages.remove(this.rainStage);
	}

	show(visible: any) {
		this.rainStage.enabled = visible;
	}

	// 优化显示隐藏方式，减少内存占用
	optShow() {
		// undefined 未定义 true 已销毁 false 未销毁
		if (this.rainStage?.isDestroyed() ?? '1' === '1') {
			this.init();
		} else {
			this.destroy();
		}
	}

	rain() {
		return `
      uniform sampler2D colorTexture;
      varying vec2 v_textureCoordinates;
      uniform float tiltAngle;
      uniform float rainSize;
      uniform float rainSpeed;

      float hash(float x) {
          return fract(sin(x * 133.3) * 13.13);
      }

      void main(void) {
          float time = czm_frameNumber / rainSpeed;
          vec2 resolution = czm_viewport.zw;
          vec2 uv = (gl_FragCoord.xy * 2. - resolution.xy) / min(resolution.x, resolution.y);
          vec3 c = vec3(.6, .7, .8);
          float a = tiltAngle;
          float si = sin(a), co = cos(a);
          uv *= mat2(co, -si, si, co);
          uv *= length(uv + vec2(0, 4.9)) * rainSize + 1.;
          float v = 1. - sin(hash(floor(uv.x * 100.)) * 2.);
          float b = clamp(abs(sin(20. * time * v + uv.y * (5. / (2. + v)))) - .95, 0., 1.) * 20.;
          c *= v * b;
          gl_FragColor = mix(texture2D(colorTexture, v_textureCoordinates), vec4(c, 1), .5);
      }
    `;
	}
}
