/**
 * 导出图片功能
 */
export default (viewer: any, name = 'IsCesium-pic') => {
	function saveToImage() {
		// 不写会导出为黑图
		viewer.render();

		const canvas = viewer.scene.canvas;
		const image = canvas.toDataURL('image/png').replace('image/png', 'image/octet-stream');

		const link = document.createElement('a');
		const blob = dataURLtoBlob(image);
		const objurl = URL.createObjectURL(blob);
		link.download = `${name}.png`;
		link.href = objurl;
		link.click();
	}

	function dataURLtoBlob(dataurl: any) {
		const arr = dataurl.split(',');
		const mime = arr[0].match(/:(.*?);/)[1];
		const bstr = atob(arr[1]);
		let n = bstr.length;
		const u8arr = new Uint8Array(n);

		while (n--) {
			u8arr[n] = bstr.charCodeAt(n);
		}
		return new Blob([u8arr], {
			type: mime,
		});
	}
	// 调用
	saveToImage();
};
