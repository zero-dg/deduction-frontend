import IsCesium from '../IsCesium';

/**
 * @description: 二维鹰眼地图功能
 * @param {*}
 * @return {*}
 */
class HawkEye2DMap {
	_viewer: any;
	_hawkEyeMap: any;
	_isMainMapTrigger!: boolean;
	_isEyeMapTrigger!: boolean;
	container: any;

	constructor(viewer: any) {
		// 主图
		this._viewer = viewer;
		// 鹰眼图
		this._hawkEyeMap = null;
		// 实例化挂载
		this._init();
	}

	// 初始化函数
	_init() {
		this._divInit();
		this._mapInit();
		this.container = this._hawkEyeMap.container;
	}

	// 动态创建div,及div初始化
	_divInit() {
		const hawkEyeDiv = document.createElement('div');
		hawkEyeDiv.setAttribute('id', 'hawkEye2dMap');
		hawkEyeDiv.style.cssText = `
      position: absolute;
      left: 10px;
      top: 10px;
      border-radius: 50%;
      height: 160px;
      width: 160px;
      overflow: hidden;
      border: 2px solid #002FA7;`;
		this._viewer._container.appendChild(hawkEyeDiv);
		return hawkEyeDiv;
	}

	// 初始化地图
	_mapInit() {
		this._hawkEyeMap = new IsCesium.Viewer('hawkEye2dMap', {
			geocoder: false,
			homeButton: false,
			sceneModePicker: false,
			baseLayerPicker: false,
			navigationHelpButton: false,
			animation: false,
			timeline: false,
			fullscreenButton: false,
			// 鹰眼地图中设置为二维地图
			sceneMode: IsCesium.SceneMode.SCENE2D,
		});
		this._hawkEyeMap.cesiumWidget.creditContainer.style.display = 'none';
		this._hawkEyeMap.scene.backgroundColor = IsCesium.Color.TRANSPARENT;
		this._hawkEyeMap.imageryLayers.removeAll();

		// 高德道路地图 与WSG84地图有偏移差距
		this._hawkEyeMap.imageryLayers.addImageryProvider(
			new IsCesium.UrlTemplateImageryProvider({
				url: 'http://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}',
				subdomains: ['1', '2', '3', '4'],
				minimumLevel: 3,
				maximumLevel: 18,
			})
		);

		// 引起事件监听的相机变化幅度
		this._viewer.camera.percentageChanged = 0.02;
		this._hawkEyeMap.camera.percentageChanged = 0.5;

		this._bindEvent();
	}
	// 绑定事件
	_bindEvent() {
		// 鹰眼与主图同步
		this._viewer.camera.changed.addEventListener(this._syncEyeMap, this);
		// 第一次刷新渲染时联动
		this._viewer.scene.preRender.addEventListener(this._syncEyeMap, this);
	}

	// 同步主图与鹰眼地图
	_syncEyeMap() {
		// 监听主图
		new IsCesium.ScreenSpaceEventHandler(this._viewer.canvas).setInputAction(() => {
			this._isMainMapTrigger = true;
			this._isEyeMapTrigger = false;
		}, IsCesium.ScreenSpaceEventType.MOUSE_MOVE);

		// 但当鹰眼图为二维地图时，则不能直接设置
		const viewCenter = new IsCesium.Cartesian2(
			// Math.floor取整函数
			Math.floor(this._viewer.canvas.clientWidth / 2),
			Math.floor(this._viewer.canvas.clientHeight / 2)
		);
		// pickEllipsoid用于将屏幕坐标转换为世界坐标
		const viewCenterPos = this._viewer.scene.camera.pickEllipsoid(viewCenter);
		if (!viewCenterPos) {
			return false;
		}

		// postionWC：标准世界坐标系坐标
		const distance = IsCesium.Cartesian3.distance(viewCenterPos, this._viewer.scene.camera.positionWC);
		this._hawkEyeMap.scene.camera.lookAt(viewCenterPos, new IsCesium.Cartesian3(0.0, 0.0, distance));
	}
}

export default HawkEye2DMap;
