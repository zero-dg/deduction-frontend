import IsCesium from '../IsCesium';
import HawkEye2DMap from './HawkEye2DMap';
import SaveImage from './SaveImage';

const toolsList = {
	HawkEye2DMap,
	SaveImage,
};

// 相关插件类
IsCesium['Tools'] = {};

Object.entries(toolsList).forEach(([key, value]: any) => {
	IsCesium.Tools[key] = value;
});
