import IsCesium from '../../IsCesium';
import EllipsoidTrailMaterial from '../shader/ellipsoid/EllipsoidTrailMaterial.glsl?raw';
import EllipsoidElectricMaterial from '../shader/ellipsoid/EllipsoidElectricMaterial.glsl?raw';

/* 轨迹球体材质 */
IsCesium.Material.EllipsoidTrailType = 'EllipsoidTrail';
IsCesium.Material._materialCache.addMaterial(IsCesium.Material.EllipsoidTrailType, {
	fabric: {
		type: IsCesium.Material.EllipsoidTrailType,
		uniforms: {
			color: new IsCesium.Color(1.0, 0.0, 0.0, 1),
			speed: 5.0,
		},
		source: EllipsoidTrailMaterial,
	},
	translucent: function (material: any) {
		return true;
	},
});

/* 电弧球体材质 */
IsCesium.Material.EllipsoidElectricType = 'EllipsoidElectric';
IsCesium.Material._materialCache.addMaterial(IsCesium.Material.EllipsoidElectricType, {
	fabric: {
		type: IsCesium.Material.EllipsoidTrailType,
		uniforms: {
			color: new IsCesium.Color(1.0, 0.0, 0.0, 1),
			speed: 5.0,
		},
		source: EllipsoidElectricMaterial,
	},
	translucent: function (material: any) {
		return true;
	},
});
