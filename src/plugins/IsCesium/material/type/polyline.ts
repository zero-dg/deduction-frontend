import IsCesium from '../../IsCesium';
import LineSpriteTrailMaterial from '../shader/polyline/PolylineSpriteTrailMaterial.glsl?raw';
import LineLightingTrailMaterial from '../shader/polyline/PolylineLightingTrailMaterial.glsl?raw';
import LineImageTrailMaterial from '../shader/polyline/PolylineImageTrailMaterial.glsl?raw';

/* 精灵线所用材质 */
IsCesium.Material.PolylineSpriteTrailType = 'PolylineSpriteTrail';
IsCesium.Material._materialCache.addMaterial(IsCesium.Material.PolylineSpriteTrailType, {
	fabric: {
		type: IsCesium.Material.PolylineSpriteTrailType,
		uniforms: {
			// color: new IsCesium.Color(1, 0, 0, 0.5),
			image: '',
			// transparent: true,
			time: 20,
		},
		source: LineSpriteTrailMaterial,
	},
	translucent: function (material: any) {
		return true;
	},
});

/* 流光线所用材质 */
IsCesium.Material.PolylineLightingTrailType = 'PolylineLightingTrail';
IsCesium.Material._materialCache.addMaterial(IsCesium.Material.PolylineLightingTrailType, {
	fabric: {
		type: IsCesium.Material.PolylineLightingTrailType,
		uniforms: {
			color: new IsCesium.Color(1.0, 0.0, 0.0, 0.7),
			image: IsCesium.Material.DefaultImageId,
			speed: 3.0,
		},
		source: LineLightingTrailMaterial,
	},
	translucent: function (material: any) {
		return true;
	},
});

/* 图片轨迹线 */
IsCesium.Material.PolylineImageTrailType = 'PolylineImageTrail';
IsCesium.Material._materialCache.addMaterial(IsCesium.Material.PolylineImageTrailType, {
	fabric: {
		type: IsCesium.Material.PolylineImageTrailType,
		uniforms: {
			color: new IsCesium.Color(1.0, 0.0, 0.0, 0.7),
			image: IsCesium.Material.DefaultImageId,
			speed: 1,
			repeat: new IsCesium.Cartesian2(1, 1),
		},
		source: LineImageTrailMaterial,
	},
	translucent: function (material: any) {
		return true;
	},
});
