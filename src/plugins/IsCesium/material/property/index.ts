import IsCesium from '../../IsCesium';

// 球体
import EllipsoidTrailMaterialProperty from './ellipsoid/EllipsoidTrailMaterialProperty';
import EllipsoidElectricMaterialProperty from './ellipsoid/EllipsoidElectricMaterialProperty';

// 线体
import PolylineSpriteTrailMaterialProperty from './polyline/PolylineSpriteMaterialProperty';
import PolylineLightingTrailMaterialProperty from './polyline/PolylineLightingTrailMaterialProperty';
import PolylineImageTrailMaterialProperty from './polyline/PolylineImageTrailMaterialProperty';

// 材质原型
const materialList = {
	EllipsoidTrailMaterialProperty,
	EllipsoidElectricMaterialProperty,
	PolylineSpriteTrailMaterialProperty,
	PolylineLightingTrailMaterialProperty,
	PolylineImageTrailMaterialProperty,
};

// 挂载到自定义对象上
Object.entries(materialList).forEach(([key, value]: any) => {
	IsCesium[key] = value;
});
