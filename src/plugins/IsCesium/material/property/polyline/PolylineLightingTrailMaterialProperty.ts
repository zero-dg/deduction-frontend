import MaterialProperty from '../MaterialProperty';
import IsCesium from '@/plugins/IsCesium/IsCesium';
import { JulianDate } from 'cesium';

/**
 * 发光轨迹线
 */
class PolylineLightingTrailMaterialProperty extends MaterialProperty {
	image: any;

	constructor(options = {}) {
		super(options);
		this.image = IsCesium.ASSETS.LIGHTING;
	}

	getType(time: JulianDate) {
		return IsCesium.Material.PolylineLightingTrailType;
	}

	getValue(time: JulianDate, result: any) {
		if (!result) {
			result = {};
		}
		result.color = this.color;
		result.image = this.image;
		result.speed = this.speed;
		return result;
	}

	equals(other: any) {
		return this === other || other instanceof PolylineLightingTrailMaterialProperty;
	}
}

Object.defineProperties(PolylineLightingTrailMaterialProperty.prototype, {
	isConstant: {
		get: function () {
			return true;
		},
	},
	color: IsCesium.createPropertyDescriptor('color'),
	speed: IsCesium.createPropertyDescriptor('speed'),
	image: IsCesium.createPropertyDescriptor('image'),
});

export default PolylineLightingTrailMaterialProperty;
