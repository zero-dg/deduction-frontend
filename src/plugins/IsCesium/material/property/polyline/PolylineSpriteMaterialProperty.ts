import IsCesium from '@/plugins/IsCesium/IsCesium';
import { JulianDate } from 'cesium';
import MaterialProperty from '../MaterialProperty';

/**
 * 精灵线
 */
class PolylineSpriteTrailMaterialProperty extends MaterialProperty {
	private _time: number;
	image: any;

	constructor(options: any = {}) {
		super(options);
		this.image = options.image || IsCesium.ASSETS.SPRITE_BLUE;
		this._time = performance.now();
	}

	getType(time: JulianDate) {
		return IsCesium.Material.PolylineSpriteTrailType;
	}

	getValue(time: JulianDate, result: any) {
		if (!result) {
			result = {};
		}
		result.image = this.image;
		result.time = ((performance.now() - this._time) % this.speed) / this.speed;
		return result;
	}

	equals(e: any) {
		return this === e || e instanceof PolylineSpriteTrailMaterialProperty;
	}
}

Object.defineProperties(PolylineSpriteTrailMaterialProperty.prototype, {
	isConstant: {
		get: function () {
			return true;
		},
	},
	definitionChanged: {
		get: function () {
			return this._definitionChanged;
		},
	},
	color: IsCesium.createPropertyDescriptor('color'),
	speed: IsCesium.createPropertyDescriptor('speed'),
	image: IsCesium.createPropertyDescriptor('image'),
});

export default PolylineSpriteTrailMaterialProperty;
