import IsCesium from '@/plugins/IsCesium/IsCesium';
import { JulianDate } from 'cesium';
import MaterialProperty from '../MaterialProperty';

/**
 * 图片轨迹线
 */
class PolylineImageTrailMaterialProperty extends MaterialProperty {
	image: any;
	repeat: any;

	constructor(options: any = {}) {
		super(options);
		this.image = options.image;
		this.repeat = new IsCesium.Cartesian2(options.repeat?.x || 1, options.repeat?.y || 1);
	}

	getType(time: JulianDate) {
		return IsCesium.Material.PolylineImageTrailType;
	}

	getValue(time: JulianDate, result: any) {
		if (!result) {
			result = {};
		}
		result.color = this.color;
		result.image = this.image;
		result.repeat = this.repeat;
		result.speed = this.speed;
		return result;
	}

	equals(other: this) {
		return this === other || other instanceof PolylineImageTrailMaterialProperty;
	}
}

Object.defineProperties(PolylineImageTrailMaterialProperty.prototype, {
	isConstant: {
		get: function () {
			return true;
		},
	},
	color: IsCesium.createPropertyDescriptor('color'),
	speed: IsCesium.createPropertyDescriptor('speed'),
	image: IsCesium.createPropertyDescriptor('image'),
	repeat: IsCesium.createPropertyDescriptor('repeat'),
});

export default PolylineImageTrailMaterialProperty;
