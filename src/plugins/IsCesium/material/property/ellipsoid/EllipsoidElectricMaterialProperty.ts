import IsCesium from '@/plugins/IsCesium/IsCesium';
import MaterialProperty from '../MaterialProperty';

/**
 * 电弧球体
 */
class EllipsoidElectricMaterialProperty extends MaterialProperty {
	constructor(options = {}) {
		super(options);
	}

	getType(time: any) {
		return IsCesium.Material.EllipsoidElectricType;
	}

	getValue(time: any, result: { color?: any; speed?: any }) {
		if (!IsCesium.defined(result)) {
			result = {};
		}
		result.color = this.color;
		result.speed = this.speed;
		return result;
	}

	equals(other: this) {
		return this === other || other instanceof EllipsoidElectricMaterialProperty;
	}
}

Object.defineProperties(EllipsoidElectricMaterialProperty.prototype, {
	color: IsCesium.createPropertyDescriptor('color'),
	speed: IsCesium.createPropertyDescriptor('speed'),
});

export default EllipsoidElectricMaterialProperty;
