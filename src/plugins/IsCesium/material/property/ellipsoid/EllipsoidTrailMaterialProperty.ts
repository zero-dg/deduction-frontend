import IsCesium from '@/plugins/IsCesium/IsCesium';
import MaterialProperty from '../MaterialProperty';

/**
 * 轨迹球体
 */
class EllipsoidTrailMaterialProperty extends MaterialProperty {
	constructor(options = {}) {
		super(options);
	}

	getType(time: any) {
		return IsCesium.Material.EllipsoidTrailType;
	}

	getValue(time: any, result: any) {
		if (!IsCesium.defined(result)) {
			result = {};
		}
		result.color = this.color;
		result.speed = this.speed;
		return result;
	}

	equals(other: this) {
		return this === other || other instanceof EllipsoidTrailMaterialProperty;
	}
}

Object.defineProperties(EllipsoidTrailMaterialProperty.prototype, {
	color: IsCesium.createPropertyDescriptor('color'),
	speed: IsCesium.createPropertyDescriptor('speed'),
});

export default EllipsoidTrailMaterialProperty;
