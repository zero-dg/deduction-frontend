/**
 * material(材质)原型包
 */
import IsCesium from '../../IsCesium';

class MaterialProperty {
	_definitionChanged: any;
	color: any;
	speed: any;

	constructor(options: any = {}) {
		this._definitionChanged = new IsCesium.Event();
		this.color = options.color || IsCesium.Color.fromBytes(255, 0, 0, 250);
		this.speed = options.speed || 10;
	}

	get isConstant() {
		return false;
	}

	get definitionChanged() {
		return this._definitionChanged;
	}

	getType(time: any) {
		return null;
	}

	getValue(time: any, result: any) {
		result = IsCesium.defaultValue(result, {});
		return result;
	}

	equals(other: this) {
		return this === other;
	}
}

export default MaterialProperty;
