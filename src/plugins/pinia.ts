// https://www.npmjs.com/package/pinia-plugin-persistedstate 持久化存储插件
import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';

const pinia = createPinia(); // 初始化Pinia
pinia.use(piniaPluginPersistedstate); // 激活Pinia持久化插件 默认存储在local

export default pinia;
