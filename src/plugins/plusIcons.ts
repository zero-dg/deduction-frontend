import { App } from 'vue';
import * as ElIcons from '@element-plus/icons-vue';

export default (app: App) => {
	for (const key in ElIcons) {
		app.component(key, ElIcons[key]);
	}
};
