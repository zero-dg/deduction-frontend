/**
 * 全局组件或插件导出
 */
import { App } from 'vue';
import router from '@/router';
import { i18n } from '@/i18n';

import registerPlusIcons from './plusIcons';
import isGlobalCompoenets from './globalComponents';
import pinia from './pinia';

import 'element-plus/dist/index.css';

export default (app: App) => {
	registerPlusIcons(app); // 注册element-plus图标组件
	app.use(pinia); // 挂载Pinia实例 全局存储
	app.use(router).use(i18n); // 路由、国际化
	app.use(isGlobalCompoenets); // 自定义组件全局注册
};
