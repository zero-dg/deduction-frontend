/**
 * 这里引入需要全局注册的自定义组件
 */
import { App } from 'vue';

// element组件二次封装
import IsTable from '@/components/IsTable/index.vue';

//转化为对象后批量注册
const comps = { IsTable };

//批量注册
const components = {
	install: (app: App) => {
		Object.entries(comps).forEach(([name, comp]) => {
			app.component(name, comp);
		});
	},
};

//导出全局注册
export default components;
