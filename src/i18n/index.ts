import { createI18n } from 'vue-i18n';
import { Local } from '@/utils';
// element-plus
import zhCnLocale from 'element-plus/lib/locale/lang/zh-cn';
import enLocale from 'element-plus/lib/locale/lang/en';
// 插件多个页面公用
import nextZhCn from './lang/zh-cn';
import nextEn from './lang/en';

// 每个页面的
import loginZhcn from './pages/login/zh-cn';
import loginEn from './pages/login/en';
import thirdPartyZhcn from './pages/third-party/zh-cn';
import thirdPartyEn from './pages/third-party/en';

// 定义语言国际化内容 zh-cn en
const messages = {
	[zhCnLocale.name]: {
		...zhCnLocale,
		ismsg: { ...nextZhCn, ...loginZhcn, ...thirdPartyZhcn },
	},
	[enLocale.name]: {
		...enLocale,
		ismsg: { ...nextEn, ...loginEn, ...thirdPartyEn },
	},
};

export const i18n = createI18n({
	legacy: false,
	silentTranslationWarn: true,
	missingWarn: false,
	silentFallbackWarn: true,
	fallbackWarn: false,
	locale: Local.get('themeConfig')?.globalI18n || 'zh-cn',
	fallbackLocale: zhCnLocale.name,
	messages,
});
