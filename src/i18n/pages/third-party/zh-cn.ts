export default {
	countup: {
		name: '数字滚动组件',
		reset: '重置',
		start: '开始',
		pauseResume: '暂停/继续',
		update: '更新',
	},
};
