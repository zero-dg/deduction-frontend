export default {
	countup: {
		name: 'Digital scroll assembly',
		reset: 'Reset',
		start: 'Start',
		pauseResume: 'Pause/Resume',
		update: 'Update',
	},
};
