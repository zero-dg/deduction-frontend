export default {
	login: {
		email: '邮箱',
		emailPrompt: '请输入邮箱',
		emailCheck: '邮箱格式错误',
		password: '密码',
		passwordPrompt: '请输入密码',
		submit: '登录',
		reset: '重置',
	},
};
