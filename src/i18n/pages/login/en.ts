export default {
	login: {
		email: 'Email',
		emailPrompt: 'Please input email',
		emailCheck: 'Email format error',
		password: 'Password',
		passwordPrompt: 'Please input password',
		submit: 'Login',
		reset: 'Reset',
	},
};
