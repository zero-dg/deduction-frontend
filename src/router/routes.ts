import { RouteRecordRaw } from 'vue-router';
/**
 * 定义动态路由
 * @params 路由meta对象参数说明
 * meta: {
 *      title:          菜单栏及 tagsView 栏、菜单搜索名称（国际化）
 *      isHide：        是否隐藏此路由
 *      roles：         当前路由权限标识，取角色管理。控制路由显示、隐藏。超级管理员：admin 普通角色：common
 *      icon：          菜单图标
 * 			isLink:					外链设置
 * }
 * @returns 返回路由菜单数据
 */
export const dynamicRoutes: Array<any> = [
	{
		// 首页
		path: '/',
		name: 'homeView',
		component: () => import('@/views/HomeView.vue'),
		meta: {
			title: 'ismsg.router.home',
			isHide: false,
			roles: ['admin', 'common'],
			icon: 'HomeFilled',
		},
	},
	{
		// 第三方组件
		path: '/packages',
		name: 'packages',
		component: () => import('@/layout/container/ParentContainer.vue'),
		meta: {
			title: 'ismsg.router.npmPackages',
			isHide: false,
			roles: ['admin', 'common'],
			icon: 'Box',
		},
		children: [
			{
				path: '/packages/countup',
				name: 'countup',
				component: () => import('@/views/third-party/CountupNum.vue'),
				meta: {
					title: 'ismsg.router.countup',
					isHide: false,
					roles: ['admin', 'common'],
					icon: 'Stopwatch',
				},
			},
		],
	},
	{
		// 自定义封装组件
		path: '/customComponents',
		name: 'customComponents',
		component: () => import('@/layout/container/ParentContainer.vue'),
		meta: {
			title: 'ismsg.router.customComp',
			isHide: false,
			roles: ['admin', 'common'],
			icon: 'CollectionTag',
		},
		children: [
			{
				path: '/customComponents/dragIsTable',
				name: 'dragIsTable',
				component: () => import('@/views/custom-components/DragTable/DragIsTable.vue'),
				meta: {
					title: 'ismsg.router.dragTable',
					isHide: false,
					roles: ['admin', 'common'],
					icon: 'Tickets',
				},
			},
		],
	},
	{
		// 可视化相关
		path: '/visualization',
		name: 'visualization',
		component: () => import('@/layout/container/ParentContainer.vue'),
		meta: {
			title: 'ismsg.router.visualization',
			isHide: false,
			roles: ['admin', 'common'],
			icon: 'View',
		},
		children: [
			{
				path: '/visualization/x6',
				name: 'x6',
				component: () => import('@/views/visualization/ant-v/x6/X6View.vue'),
				meta: {
					title: 'ismsg.router.x6',
					isHide: false,
					roles: ['admin', 'common'],
					icon: 'DataLine',
				},
			},
			{
				path: '/visualization/cesium',
				name: 'cesium',
				component: () => import('@/views/visualization/cesium/CesiumView.vue'),
				meta: {
					title: 'ismsg.router.cesium',
					isHide: false,
					roles: ['admin', 'common'],
					icon: 'PictureRounded',
				},
			},
			{
				path: '/visualization/echarts',
				name: 'echarts',
				component: () => import('@/views/visualization/echarts/EchartsView.vue'),
				meta: {
					title: 'ismsg.router.echarts',
					isHide: false,
					roles: ['admin', 'common'],
					icon: 'PieChart',
				},
			},
			{
				path: '/visualization/openlayers',
				name: 'openlayers',
				component: () => import('@/views/visualization/openlayers/OpenlayersView.vue'),
				meta: {
					title: 'ismsg.router.openlayers',
					isHide: false,
					roles: ['admin', 'common'],
					icon: 'Picture',
				},
			},
		],
	},
	{
		// 系统说明-外链
		path: '/systemBook',
		name: 'systemBook',
		meta: {
			title: 'ismsg.router.docsLink',
			isHide: false,
			roles: ['admin', 'common'],
			icon: 'Management',
			isLink: 'https://zero-dg.gitee.io/is-deduction-docs/',
		},
	},
];

/**
 * 定义静态路由（默认路由）
 * 此路由不要动，前端添加路由的话，请在 `dynamicRoutes 数组` 中添加
 * @returns 返回路由菜单数据
 */
export const staticRoutes: Array<RouteRecordRaw> = [
	// 布局容器
	{
		path: '/',
		name: 'home',
		component: () => import('@/layout/index.vue'),
	},
	{
		path: '/login',
		name: 'login',
		component: () => import('@/views/login/LoginView.vue'),
		meta: {
			title: 'ismsg.router.login',
		},
	},
	{
		path: '/401',
		name: 'noPower',
		component: () => import('@/views/error/401.vue'),
		meta: {
			title: 'ismsg.router.noPower',
		},
	},
	{
		path: '/:path(.*)*', //	path: '/:catchAll(.*)', // 不识别的path自动匹配
		name: 'notFound',
		component: () => import('@/views/error/404.vue'),
		meta: {
			title: 'ismsg.router.notFound',
		},
	},
];
