/**
 * 路由控制方法
 */
import { dynamicRoutes } from './routes';
import { Session } from '@/utils';
import { RouteRecordRaw } from 'vue-router';

/**
 * 初始化路由数据
 * 这里可以通过参数来控制是前端传入还是后端传入
 */
export const initRouterPath = async (router: any) => {
	if (!Session.get('token')) return;
	// 增添子路由
	dynamicRoutes.forEach((item: RouteRecordRaw) => {
		router.addRoute('home', item);
	});
};

/**
 * 获取菜单路由列表，过滤出需要显示的菜单
 * @returns Array[]
 */
export const getRouteList = () => {
	//递归过滤
	const recursionFilter = (list: Array<RouteRecordRaw>) => {
		return list.filter((item: RouteRecordRaw) => {
			if (!item?.meta?.isHide && item.children && item.children.length > 0) {
				item.children = recursionFilter(item.children);
			}
			return !item?.meta?.isHide;
		});
	};
	// 返回过滤结果
	return recursionFilter(dynamicRoutes);
};
