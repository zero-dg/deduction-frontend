import { watchEffect } from 'vue';
import { createRouter, createWebHashHistory } from 'vue-router';
import { Session } from '@/utils';
import { staticRoutes } from './routes';
import { initRouterPath } from './controlRouter';

//多语言国际化
import { i18n } from '@/i18n';
// 进度条插件
import nprogress from 'nprogress';
import 'nprogress/nprogress.css';

//国际化
const $t = i18n.global.t;
//创建路由实例
const router = createRouter({
	history: createWebHashHistory(),
	routes: staticRoutes,
});

// 刷新初始化路由
initRouterPath(router);

//路由加载前
router.beforeEach(async (to: any, from, next) => {
	if (!to.meta.isLink) {
		nprogress.start(); //进度条加载 开始
		// 浏览器标题切换
		watchEffect(() => {
			if (to.meta.title) document.title = $t(to.meta.title) + ' | ' + $t('ismsg.sysName');
		});
	}

	const token = Session.get('token');
	if (to.path === '/login' && !token) {
		next();
	} else {
		if (!token) {
			next(`/login?redirect=${to.path}&params=${JSON.stringify(to.params ? to.params : to.query)}`);
			Session.clear();
		} else if (to.meta.isLink) {
			window.open(to.meta.isLink, '_blank');
			next(from);
		} else if (to.path === '/login') {
			next('/');
		} else {
			next();
		}
	}
});

//路由加载后
router.afterEach(() => {
	nprogress.done(); //进度条加载 结束
});

export default router;
